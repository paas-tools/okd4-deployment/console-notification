# Console Notification

In OKD is possible to deploy a banner (named notification) in the console through the CRD `ConsoleNotification` (see [upstream docs](https://docs.okd.io/4.12/web_console/customizing-the-web-console.html#creating-custom-notification-banners_customizing-web-console)).

This repo contains a helm chart to deploy `ConsoleNotification` resources in OKD. The helm chart is aware of the flavor of the cluster (`app-catalogue`, `drupal`, `paas`, `webeos`) and will deploy one or more notifications depending on the flavor.

## How to use it

To add a notification to the console, you can add an item in the `notifications` list in the `values.yaml` file. 
The item should have the following structure:
- `text` (**required**): the text of the notification
- `clusterToDeploy` (**required**): the list of flavors where the notification should be deployed. The possible values are `app-catalogue`, `drupal`, `paas`, `webeos`
- `location`: where the notification should be displayed. The possible values are `BannerTop`, `BannerBottom` and `BannerTopBottom`. The default value is `BannerTop`.
- `link`: if you want to add a link, it will be added at the beginning of the text of the notification. If you want to add a link, the following subfields are **required**:
  - `text`: the text of the link
  - `href`: the url of the link
- `color`: color of the text (default: `#fff`)
- `backgroundColor`: background color of the notification (default: `#c80000`)

An example of well-formed values.yaml is the following:
```yaml
notifications:
 - text: Example banner for PaaS and App Catalogue.
   location: BannerTop
   link:
    href: 'https://cern.service-now.com/service-portal?id=outage&n=OTG0078282'
    text: OTG0078282
   color: '#d8f'
   backgroundColor: '#c80000'
   clusterToDeploy: [paas,paas-stg,app-catalogue]
 - text: Example banner for webEOS and Drupal.
   clusterToDeploy: [webeos,drupal]
```

If a notification is not needed anymore, you can just remove the item from the list.

## How to deploy it

The content of the branch `master` is automatically deployed from the ArgoCD application `console-notification` in the namespace `openshift-console`. 

To push the changes to the cluster, you only need to create a MR and merge it to `master`. The ArgoCD application will automatically deploy the changes.
